package com.example.whoisamillionaire.activity;

import android.app.ListActivity;
import android.os.Bundle;

import com.example.whoisamillionaire.R;
import com.example.whoisamillionaire.adapter.HighScoreAdapter;
import com.example.whoisamillionaire.manager.DatabaseManager;
import com.example.whoisamillionaire.manager.MusicManager;


public class HighScoreActivity extends ListActivity {
    private MusicManager musicManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_high_score);

        musicManager = new MusicManager(this);
        DatabaseManager databaseManager = new DatabaseManager(this);
        getListView().setAdapter(new HighScoreAdapter(this, databaseManager.getHighScore()));
    }

    @Override
    protected void onPause() {
        super.onPause();
        musicManager.pauseBgMusic();
    }

    @Override
    protected void onResume() {
        super.onResume();
        musicManager.resumeBgMusic();
    }
}
